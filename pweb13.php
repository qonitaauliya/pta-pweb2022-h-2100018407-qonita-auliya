<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width-device-width, initial-scale-1.0">
	<link href="https://fonts.googleapis.com/css2?family=Quicksand:wght@500&display=swap" rel="stylesheet">
	<title>Guestbook</title>
</head>
<body>
	<form method="POST" action="<?php echo $_SERVER['PHP_SELF'];?>">
	<table width="366" border="0" align="center">
        <tr><td colspan="2">
            <div align="center">
                <h2>Form Validasi</h2>
                <p>.....................................................</p>
            </div>
        </td></tr>
        <tr><td width="254">
            <input class="name" name="name" type="text" id="TxtNama" size="42" maxlength="25" placeholder="Nama">
        </td></tr>
        <tr><td>
            <input class="prodi" name="prodi" type="text" id="TxtProdi" size="42" maxlength="35" placeholder="Program Studi">
        </td></tr>
        <tr><td>
            <input class="nim" name="nim" type="text" id="TxtNim" size="42" maxlength="30" placeholder="NIM">
        </td></tr>
        <tr><td>
            <input class="alamat" name="alamat" type="text" id="TxtAlamat" size="42" maxlength="20" placeholder="Alamat">
        </td></tr>
        <tr><td>
            <textarea name="kritiksaran" placeholder="Kritik & Saran"></textarea>
        </td></tr>
        <tr><td colspan="2">
            <div align="center">
                <input class="button" type="submit" name="submit" value="Kirim">
            </div>
        </td></tr>
    </table>

	<div class="container">
		<?php
			if(isset($_POST['submit'])){
                $name = $_POST['name'];
                $prodi = $_POST['prodi'];
                $nim = $_POST['nim'];
                $alamat = $_POST['alamat'];
                $kritiksaran = $_POST['kritiksaran'];

                echo "<div class='tampilan'>
                Nama Anda           : $name <br>
                Program Studi Anda  : $prodi <br>
                Nim Anda            : $nim <br>
                Alamat Anda         : $alamat <br>
                Kritik & saran Anda : $kritiksaran <br>
                ";
            }
		?>
	</div>
</body>

<style type="text/css">
	body {
        background: #b0c4de;
        color: #ffb6c1;
        font-family: Arial;
    }
    h3 {
        margin-bottom: -5px;
    }
    h2 {
        font-size: 1.5em;
        color: #778899;
    }
    table {
        background: #ffffff;
        border-radius: 5px;
        padding: 20px;
        margin-top: 20px;
    }
    .container {
        width: 800px;
        text-align: left;
        margin: 40px auto;
        color: black;
        background: #ffffff;
        border-radius: 5px;
        padding: 20px;
        margin-top: 20px;
    }
    tr {
        height: 35px;
    }
    textarea {
        width: 348px;
        height: 100px;
        padding: 10px;
        font-family: Arial;
        font-size: 12px;
    }
    input.name {
        background: url(images/user.png)no-repeat 7px 9px;
        border: 1px solid #c9c9c9;
        border-radius: 3px;
        height: 25px;
        padding: 8px;
        padding-left: 37px;
        margin-bottom: 8px;
        transition: 1s all;
        -moz-transition: 1s all;
        -o-transition: 1s all;
    }
    input.prodi {
        background: url(images/maps.png)no-repeat 7px 9px;
        border: 1px solid #c9c9c9;
        border-radius: 3px;
        height: 25px;
        padding: 8px;
        padding-left: 37px;
        margin-bottom: 8px;
        transition: 1s all;
        -moz-transition: 1s all;
        -o-transition: 1s all;
    }
    input.nim {
        background: url(images/mail.png)no-repeat 7px 9px;
        border: 1px solid #c9c9c9;
        border-radius: 3px;
        height: 25px;
        padding: 8px;
        padding-left: 37px;
        margin-bottom: 8px;
        transition: 1s all;
        -moz-transition: 1s all;
        -o-transition: 1s all;
    }
    input.alamat {
        background: url(images/kota.png)no-repeat 7px 9px;
        border: 1px solid #c9c9c9;
        border-radius: 3px;
        height: 25px;
        padding: 8px;
        padding-left: 37px;
        margin-bottom: 8px;
        transition: 1s all;
        -moz-transition: 1s all;
        -o-transition: 1s all;
    }
    input.kritiksaran {
        background: url(images/mail.png)no-repeat 7px 9px;
        border: 1px solid #c9c9c9;
        border-radius: 3px;
        height: 25px;
        padding: 8px;
        padding-left: 37px;
        margin-bottom: 8px;
        transition: 1s all;
        -moz-transition: 1s all;
        -o-transition: 1s all;
    }
    input:focus {
        border: 1px solid #26C281;
        transition: 1s all;
        -moz-transition: 1s all;
        -o-transition: 1s all;
    }
    .button {
        background: #b0c4de;
        color: #ffffff;
        width: 370px;
        margin-top: 10px;
        height: 40px;
        border: 1px solid #c9c9c9;
        border-radius: 5px;
        transition: 1s all;
        -moz-transition: 1s all;
        -o-transition: 1s all;
    }
    .button:hover {
        background: #4682b4;   
        transition: 1s all;
        -moz-transition: 1s all;
        -o-transition: 1s all;
    }
    .button-gambar {
        height: 35px;
    }
</style>
</html>